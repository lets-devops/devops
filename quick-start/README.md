欢迎加入垒知研究院！

下面信息可以帮助您尽快熟悉环境。

# 账号
您将会使用到三个内部系统账号

1. 建研集团账号  
主要用于登录建研集团系统，包括OA系统（http://oa.xmabr.com)，建研OA app等。一般为您名字的全拼，或者在全拼后加数字。如zhangsan或zhangsan2。

2. 垒知账号  
主要用于登录桌面云系统，垒知四川邮件系统。一般为您名字的全拼，或者姓全拼加上名的首字母，如zhangsan或者zhangs。

3. 研究院内部系统账号  
主要用于研究院内部IT系统，包括LDAP，Gitlab，Nextcloud，Jenkins等。一般为您名字的全拼，如zhangsan。
  
此外，我们会使用到一些云服务，请使用垒知邮箱账号注册使用。

# 系统应用

1. 邮件  
在线邮箱：腾讯企业邮箱 
客户端配置：  
服务器：imap.exmail.qq.com 
端口: 993 

2.	OA系统 oa.xmabr.com  
OA移动端下载安装 oa.xmabr.com 我的办公桌 - 短消息 - App下载  
每天需要3次打卡，早上上班（9:00 – 12:00），下午上下班(1:00 – 5:00)。  
如果忘记打卡，在72小时内，找上级在OA系统提外出申请。路径是：我的办公桌-考勤-外出登记。  

3.	桌面云  
公司配备桌面云电脑供大家使用，具体地址和使用方法，参见http://nextcloud.letsbim.net/index.php/f/10838。  
桌面云服务器地址 https://letsvdi.bim114.com（外网 https://118.122.122.42:666 ）  

4.	AP密码  
办公室wifi账号密码：LETS-AP/LETS88888888  

5.	项目管理工具TAPD(https://www.tapd.cn/)  
我们使用TAPD管理项目，邀请邮件由李春兰发送，请根据邮件指示注册。  

6.	DevOps环境 http://gitlab.letsbim.net/lets/lets-devops   
请使用吴飞发送的账号和密码，登录。  

7.	岗位清单  
每个月需建立自己的岗位清单，交上级审核。
