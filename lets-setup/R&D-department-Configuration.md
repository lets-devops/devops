# 垒知(成都)科技研究院服务器建华机房配置清单
## Gitlab服务器CentOS7.6（本地服务器）
ip 192.168.250.60 
账号 root  密码 gemhone2016   

## Jenkins服务器CentOS6.9（本地服务器）
ip 192.168.250.62
jenkins运行在/home/apache-tomcat-7.0.73/webapps目录下
账号 root  密码 gemhone2016  


## Zabbix,Nexus服务器CentOS7.6（本地服务器）
ip 192.168.250.69  
账号 root  密码 gemhone2016  

## OpenLDAP,Nextcloud服务器CentOS7.6（本地服务器）
ip 192.168.250.77  项目在/root/bim-test/目录  
运行命令（必须在/root/bim-test目录下） pm2 start index.js   
账号 root   密码 gemhone2016  

## 研究院开发服务器CentOS7.6（SonarQube）
ip 192.168.250.78   
账号 root   密码 gemhone2016  

## 研究院测试服务器CentOS7.6（本地服务器）
ip 192.168.250.79  
账号 root   密码 gemhone2016  

## 研究院自动化测试服务器CentOS8(本地服务器)
ip 192.168.250.134  
账号 root   密码 gemhone2016  





