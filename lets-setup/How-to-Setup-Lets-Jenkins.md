# 安装Jenkins平台最新版

## 1、安装jdk

rpm -ivh https://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/jdk-8u201-linux-x64.rpm

## 2、安装tomcat

wget http://mirrors.shu.edu.cn/apache/tomcat/tomcat-9/v9.0.16/bin/apache-tomcat-9.0.16.tar.gz

tar -zxvf apache-tomcat-9.0.16.tar.gz

cd apache-tomcat-9.0.16 && ./bin/startup.sh

## 3、下载最新jenkins.war放到tomcat的webapps目录下

cd ./webapps && wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war

## 4、浏览器输入http://服务器ip:8080/jenkins

按照网页提示输入密码然后跳过插件安装步骤直接登录

## 5、OpenLdap集成

在系统管理→插件管理→安装LDAP插件，在系统管理的全局安全配置添加ldap的相关信息

## 6、Jenkins自动化部署java web项目

下载插件
LDAP → Git → SSH →  Maven Integration →   Deploy to container → Email Extension

在服务器上安装jdk,git,maven并在全局工具配置中配置Jenkins服务器上的jdk,git,maven路径

## 7、CentOS7安装git2.9.5，先下载解压cd到解压目录（yum安装git路径为 which git）
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-develperl-devel

make prefix=/usr/local/git all

make prefix=/usr/local/git install

vim /etc/profile

#Jenkins

* export MAVEN_HOME=/usr/local/maven
* export PATH=$MAVEN_HOME/bin:$PATH
* export PATH=/usr/local/git/bin:$PATH

source /etc/profile

#rpm安装的jdk路径 JAVA_HOME=/usr/java/jdk1.8.0_201-amd64#

## 8、远程部署服务器conf目录下配置文件修改

Tomcat-user.xml配置添加修改如下

* \<role rolename="manager-gui" />
* \<role rolename="manager-status" />
* \<role rolename="manager-jmx" />
* \<role rolename="manager-script"/>
* \<role rolename="admin-gui"/>
* \<role rolename="admin-script"/>

* \<user username="root" password="gemhone2016" roles="manager-gui,manager-status,manager-jmx,manager-script,admin-gui,admin-script"/>

## openldap认证失败解决办法
https://blog.csdn.net/wudj810818/article/details/51426642

