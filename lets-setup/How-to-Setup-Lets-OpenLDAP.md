# CentOS7安装OpenLDAP最新版

## 1、安装vim编辑文件用和epel-release源
yum install vim  epel-release -y

## 2、同步时间，关闭seliunx和firewalld，重启服务器
ntpdate -u ntp.api.bz && sed -i '/SELINUX/s/enforcing/disabled/' /etc/selinux/config  
&& setenforce 0 && systemctl disable firewalld && systemctl stop firewalld && shutdown -r now

## 3、yum安装OpenLDAP相关的组件
yum -y install openldap-clients openldap-servers 

## 4、配置的OpenLDAP的的管理员密码
slappasswd -s 123456  
上述生成加密后的字段保存下，等会我们在配置文件中会使用到。

## 5、修改olcDatabase = {2} hdb.ldif文件 
**注意：从OpenLDAP2.4.23版本开始所有配置数据都保存在/etc/openldap/slapd.d/中，建议不再使用的slapd.conf的文件作为配置文件。**

vim /etc/openldap/slapd.d/cn\=config/olcDatabase={2}hdb.ldif

修改olcDatabase = {2} hdb.ldif文件，对于该文件增加一行

- olcRootPW: {SSHA}UpLaFI0Wkn35meshxhsYcRpnmqGvshSg，然后修改域信息：
- olcSuffix: dc=lets,dc=com
- olcRootDN: cn=root,dc=lets,dc=com

__注意：其中cn=root,dc=lets,dc=com表示的OpenLDAP管理员的用户名，而olcRootPW表示OpenLDAP的管理员的密码。__

## 6、修改olcDatabase = {1} monitor.ldif文件
vim /etc/openldap/slapd.d/cn=config/olcDatabase\=\{1\}monitor.ldif  
**注意：该修改中的dn.base是修改的OpenLDAP的的的管理员的用户名。**

## 7、验证的OpenLDAP的的基本配置，使用如下命令
slaptest -u  
当命令行中显示succeeded字段表示OpenLDAP的的基本配置是没有问题。  
启动的OpenLDAP的服务，使用如下命令  
systemctl start slapd  
systemctl enable slapd  
systemctl status slapd    
OpenLDAP的的默认监听的端口是389，通过如下命令看下是不是389端口    
netstat -antup | grep 389

## 8、配置的OpenLDAP的数据库
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG  
chown ldap:ldap -R /var/lib/ldap  
chmod 700 -R /var/lib/ldap  
ll /var/lib/ldap/  
**注意：/var/lib/ldap就是的BerkeleyDB数据库默认存储的路径。**

## 9、导入基本架构
导入基本架构，使用如下命令:
- ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
- ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
- ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif

# 创建基本组织架构

## 1、创建组织
***vim /root/base.ldif***

dn: dc=lets,dc=com    
o: lets com  
dc: lets  
objectClass: top  
objectClass: dcObject  
objectclass: organization

dn: cn=root,dc=lets,dc=com  
cn: root  
objectClass: organizationalRole  
description: Directory Manager

dn: ou=people,dc=lets,dc=com  
ou: people  
objectClass: top  
objectClass: organizationalUnit  

dn: ou=group,dc=lets,dc=com    
ou: group    
objectClass: top  
objectClass: organizationalUnit 

***vim /root/ldapuser.ldif***

dn: uid=ada,ou=people,dc=lets,dc=com  
objectClass: inetOrgPerson  
objectClass: posixAccount  
objectClass: shadowAccount  
uid: ada  
cn: Ada Catherine  
sn: Catherine  
userPassword: {SSHA}NwxXC1r3EwP9eZLEy8h5DqBY4Gbhr96R  
loginShell: /bin/bash  
uidNumber: 1000  
gidNumber: 1000  
homeDirectory: /home/users/ada  

dn: cn=jhyfb,ou=group,dc=lets,dc=com  
objectClass: posixGroup  
cn: jhyfb  
gidNumber: 1000  
memberUid: ada  

# 2、导入基础数据库
ldapadd -x -w "123456" -D "cn=root,dc=lets,dc=com" -f /root/base.ldif  
ldapadd -x -W  -D  cn=root,dc=lets,dc=com  -f /root/ldapuser.ldif  
可以使用ldapsearch命令来查看LDAP目录服务中的所有条目信息:  

ldapsearch -x -b "dc=lets,dc=com" -H ldap://127.0.0.1  
**如果要删除一个条目，可以按下面的命令操作:**  
ldapdelete -x -W -D 'cn=root,dc=lets,dc=com' "uid=ada,ou=people,dc=lets,dc=com"

通过以上的所有步骤，我们就设置好了一个LDAP目录树:  
其中基准dn dc=lets,dc=com是该树的根节点   
其下有一个管理域cn=root,dc=lets,dc=com和两个组织单元ou=people,dc=lets,dc=com及ou=group,dc=lets,dc=com

## 3、配置OpenLDAP的的日志访问功能
vim /root/loglevel.ldif 

dn: cn=config  
changetype: modify  
replace: olcLogLevel  
olcLogLevel: stats  

# 4、导入到的OpenLDAP的中，并重启的OpenLDAP的服务，如下：
ldapmodify -Y EXTERNAL -H ldapi:/// -f /root/loglevel.ldif

systemctl restart slapd  
修改rsyslog现在现在配置文件，并重启rsyslog现在现在服务，如下:  
cat >> /etc/rsyslog.conf << EOF  
local4.* /var/log/slapd.log  
EOF

## 5、重启日志服务
systemctl restart rsyslog

现在查看的OpenLDAP的日志，如下：

tail -f /var/log/slapd.log

# 安装phpldapadmin

## 1、安装httpd服务
yum install httpd -y  
systemctl enable httpd  
systemctl start httpd

## 2、安装phpldapadmin
yum install phpldapadmin -y

## 3、编辑配置文件
vim /etc/phpldapadmin/config.php

取消dn注释，注释uid行或设置直接用cn（及用户名）登录  

\$servers->setValue('login','attr','dn');   
\$servers->setValue('login','attr','cn');   
// $servers->setValue('login','attr','uid');

添加禁止匿名访问

\$servers->setValue('login','anon_bind',false);  
//$servers->setValue('login','anon_bind',true);

vim /etc/httpd/conf.d/phpldapadmin.conf

Alias /phpldapadmin /usr/share/phpldapadmin/htdocs    
Alias /ldapadmin /usr/share/phpldapadmin/htdocs    
- <Directory /usr/share/phpldapadmin/htdocs>  
-    \<IfModule mod_authz_core.c>  
       #Apache 2.4  
    Require local 修改为 Require all granted

## 3、重启httpd
systemctl restart httpd

## 4、创建项目组和成员
http://114.116.15.178/phpldapadmin

## 5、设置权限及管理员组
vim /etc/openldap/slapd.d/cn=config/olcDatabase={2}hdb.ldif

#to attrs=userPassword通过属性找到访问范围密码;  
#超级管理员也就是我们ldap配置文件里写的rootdn："cn=root,dc=jyyjy,dc=com"有写(write)权限；  
#由于管理员可能不止一个，我创建了个管理员组"ou=Admin,dc=jyyjy,dc=com"把管理员统一都放到这个组下，管理员组下的所有用户（dn.children）有写权限；  
#匿名用户(anonymous)要通过验证(auth);  
#自己(self)有对自己密码的写（write）权限，其他人(*)都没有权限(none).    
olcAccess: {0}to attrs=userPassword   
           by self write    
           by anonymous auth  
           by * read  
#管理员rootdn："cn=chenliang,ou=People,dc=jyyjy,dc=com"有写(write)权限；
#管理员组 "ou=Admin,dc=jyyjy,dc=com"成员有写(write)权限；
#其他人(*)只有读(read)权限  
olcAccess: {1}to *  
           by dn="cn=wufei,ou=people,dc=lets,dc=com" write  
           by dn.children="ou=guanli,ou=people,dc=lets,dc=com" write  
           by * read 
### 2020 opeldap

olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=extern
 al,cn=auth manage by * break
olcAccess: {1}to attrs=userPassword,shadowLastChange by self write by dn="cn
 =admin,dc=sinux,dc=com" write  by anonymous auth by * none
olcAccess: {2}to * by dn.children="ou=mcd,dc=sinux,dc=com" read by dn="cn=wufei,ou=mcd,dc=sinux,dc=com" write by * 
 none

## 6、备份和恢复
### 备份
ldapsearch -x -D 'cn=root,dc=jyyjy,dc=com' -w '123456' -b 'dc=jyyjy,dc=com' -LLL -H ldap://114.116.15.178 > ldapbackup_jyyjy.ldif
### 恢复
systemctl stop slapd  
rm -fr /var/lib/ldap/*  
slapadd -l /root/ldapbackup_jyyjy.ldif