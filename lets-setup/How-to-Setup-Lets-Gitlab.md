## 安装docker-ce最新版

### 1、如果之前安装过旧版本的Docker，请先卸载;首次安装请跳过此步骤。

yum remove docker \
docker-common \
container-selinux \
docker-selinux \
docker-engine

准备完以上步骤，就可以正式开始在Centos上安装Docker CE。

### 2、安装 yum-utils ,它可以提供 yum-config-manager 来管理repository及扩展包。

yum install -y yum-utils

### 3、设置稳定的存储库

yum-config-manager \
--add-repo \
https://download.daocloud.io/docker/linux/centos/docker-ce.repo

https://download.docker.com/linux/centos/docker-ce.repo

### 4、更新 yum 包

yum makecache fast( yum clean all)

### 5、安装最新版本的Docker CE

yum install docker-ce -y

### 6、启动Docker CE

systemctl start docker
至此Docker CE就安装完成了设置开机自启

systemctl enable docker

### 7、配置国内镜像加速器
使用我的 DaoCloud 加速器
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://8ad7943c.m.daocloud.io

cat>/etc/docker/daemon.json<<EOF
{
"registry-mirrors":["https://registry.docker-cn.com"]
}
EOF

### 8、重启Docker，就能正常下载部署镜像了。

systemctl restart docker

dockerd --debug查看异常

## 容器化部署gitlab

### 1、运行gitlab-ce最新版镜像
docker run -d --publish 443:443 --publish 80:80 --publish 23:22 --name gitlab --restart always --volume /home/gitlab/config:/etc/gitlab --volume /home/gitlab/logs:/var/log/gitlab --volume /home/gitlab/data:/var/opt/gitlab docker.io/gitlab/gitlab-ce:latest                                                                                                                                                                                                                                                                                                                                                                                                                                                                               

### 2、在gitlab.rb文件中添加

external_url "http://192.168.250.69" (url不需要加端口）

### 3、修改gitlan.rb集成OpenLdap

gitlab_rails['ldap_enabled'] = true

###! **remember to close this block with 'EOS' below**

gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
   main: # 'main' is the GitLab 'provider ID' of this LDAP server
     label: 'LDAP' 
     host: '192.168.250.77'
     port: 389
     uid: 'cn'
     bind_dn: 'cn=root,dc=lets,dc=com'
     method: 'plain' # "tls" or "ssl" or "plain"
     password: 'Gemhone_2018'
     active_directory: true
     allow_username_or_email_login: false
     lowercase_usernames: false
     block_auto_created_users: false
     base: 'ou=people,dc=lets,dc=com'
     user_filter: ''
   EOS

### 4、重启容器

docker restart 容器ID

## 配置OpenLDAP用户为管理员

### 1、进入容器

docker exec -it gitlab gitlab-rails console production

### 2、执行命令

user = User.find_by(username: 'wufei')

user.admin = true

user.save!

### 3、关闭控制台：

exit

### 4、然后更新你的gitlab服务器并重新登录账号

docker exec -it gitlab gitlab-ctl reconfigure

## 备份和恢复
vim /etc/ssh/ssh_config 

修改StrictHostKeyChecking no

GitLab自动备份  
新建备份的sh文件：在/root 下新建gitlab_backup.sh ，内容如下：  

#！ /bin/bash  
case "$1" in   
    start)  
            docker exec gitlab gitlab-rake gitlab:backup:create  
            ;;  
esac  
测试 执行gitlab_backup.sh start，如果/srv/gitlab/data/backups 下有新生成的tar包，即可说明sh文件写对了。
## 新增定时任务：使用crontab -e 进入定时任务编辑界面，新增如下内容：

0 2 * * * /root/gitlab_backup.sh start  
保存，重新加载配置和启动或重启cron服务即可。   
对于开机自启动cron服务，需要reboot验证。  

定时将备份上传到另一台服务器  
主要通过scp在Linux系统之间进行文件传输，scp是基于SSH登录的，需要我们进行免密码登录的操作。  

1、安装sshpass  
     wget http://sourceforge.net/projects/sshpass/files/sshpass/1.05/sshpass-1.05.tar.gz   
     tar xvzf sshpass-1.05.tar.gz   
     进入解压后文件夹  
     ./configure   
     make   
     make install  
此时sshpass安装完成。  

## 设置gitlab_upload.sh
在/root 下新建gitlab_upload.sh ，内容如下：  
#! /bin/bash  
backdir='/srv/gitlab/data/backups'  
latestFileName=`ls $backdir -t|head -n 1`  

cd "$backdir"  
sshpass -p P@ssw0rd scp "$latestFileName" root@114.116.15.178:/root  
新增定时任务：使用crontab -e 进入定时任务编辑界面，新增如下内容：  

0 3 * * * /root/gitlab_upload.sh start  
备份恢复  
https://blog.csdn.net/ouyang_peng/article/details/77070977

## 安装docker仓库

docker run -d -p 5000:5000 --restart=always --name registry -v /mnt/registry:/var/lib/registry  registry:2
