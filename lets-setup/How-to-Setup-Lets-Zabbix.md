# 安装zabbix3.4
## 添加Zabbix存储库
rpm -ivh https://repo.zabbix.com/zabbix/3.4/rhel/7/x86_64/zabbix-release-3.4-2.el7.noarch.rpm
## Zabbix前端需要额外的基本安装包。您需要在将运行Zabbix前端的系统中启用可选rpms的存储库
yum-config-manager --enable rhel-7-server-optional-rpms
## 要在 MySQL支持下安装Zabbix服务器
yum install zabbix-server-mysql
## 要在 MySQL支持下安装Zabbix前端
yum install zabbix-web-mysql
## mysql创建数据库（需要先安装）
shell> mysql -uroot -pgemhone2016  
mysql> create database zabbix character set utf8 collate utf8_bin;  
mysql> grant all privileges on zabbix.* to zabbix@localhost identified by 'gemhone2016';    
mysql> quit;
## 导入数据
zcat /usr/share/doc/zabbix-server-mysql-3.4.15/create.sql.gz | mysql -uzabbix -p zabbix
## 为Zabbix服务器配置数据库
#vi /etc/zabbix/zabbix_server.conf#   
DBHost = localhost   
DBName = zabbix  
zabbix DBUser = zabbix  
zabbix DBPassword = gemhone2016 
## 启动Zabbix服务器进程
service zabbix-server start  
systemctl enable zabbix-server
## Zabbix前端配置(设置正确的时区）
vi /etc/httpd/conf.d/zabbix.conf  
php_value date.timezone Asia/Shanghai(取消该行注释并修改为上海时区） 
## 在前端和SELinux(关闭）配置完成后，您需要重新启动Apache Web服务器
service httpd restart
## 代理安装及启动
yum install zabbix-agent -y  
service zabbix-agent start
## OpenLDAP集成+Email
LDAP主机
ldap://114.116.15.178
端口
389
基于 DN
ou=People,dc=jyyjy,dc=com
搜索属性
cn
绑定 DN
cn=root,dc=jyyjy,dc=com
绑定密码
测试认证
[必需为一个正确的LDAP用户]
登录
Admin

SMTP服务器
mail.gemhone.com
SMTP服务器端口
587
SMTP HELO
gemhone.com
SMTP电邮
yanfa@gemhone.com
STARTTLS(纯文本通信协议扩展)
SSL验证对端  无  
SSL验证主机  无  
认证
Username and password
用户名称
yanfa

## 添加动作+web检测+触发器
链接地址http://www.51niux.com/?id=152



 
 