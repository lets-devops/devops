# 安装python3.6+pip3+django
pip install --upgrade setuptools pip  
pip install -r requirement.txt --default-timeout=1000   
python manage.py makemigrations  
python manage.py migrate --fake  
python manage.py runserver 0.0.0.0:8000  

# 生产环境内存占用
ps -aux | sort -k4nr | head -10

# CentOS8安装docker-ce
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup  

wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-8.repo  
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-8.repo

yum install https://download.docker.com/linux/fedora/30/x86_64/stable/Packages/containerd.io-1.2.6-3.3.fc30.x86_64.rpm  

curl https://get.docker.com |env CHANNEL=stable sudo sh -s docker --mirror Aliyun
# 安装harbor
wget https://github.com/goharbor/harbor/releases/download/v2.0.1/harbor-offline-installer-v2.0.1.tgz  
yum install -y lrzsz vim telnet tar 
tar -zxvf harbor-offline-installer-v2.0.1.tgz  
cd /root/harbor   
vim harbor.yml  
vim /etc/docker/daemon.json    
systemct restart docker  
docker-compose restart  
docker login  


