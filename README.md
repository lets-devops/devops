此项目用于记录构建LETS DevOps的相关信息和资料。

## 1、Project Management
我们使用cockpit.socket管理CentOS8：https://192.168.6.166:9090  
使用portainer管理docker容器： http://192.168.6.166:9000  
使用rancher管理k8s集群：https://192.168.6.140  
使用VNC Viewer管理桌面: 192.168.6.140::5901   
使用kibana管理日志：http://192.168.6.140:5601  
使用portainer-zh管理docker容器： http://192.168.6.140:9000    
使用spug作为运维管理平台：http://192.168.3.245:70 
## 2、CODING
使用portainer管理docker容器： http://192.168.6.166:9000
## 3、OpenLDAP (yum 安装)
使用OpenLDAP集中管理用户并为其他内部应用或工具提供用户认证： http://192.168.250.77:81    
用户使用如下地址修改密码： http://192.168.250.77:81/pwd/
## 4、GitLab （docker安装部署）
使用GitLab-CE作为源代码管理：   http://192.168.250.60
## 5、Nexus （docker 安装部署）
使用nexus作为maven私服:  http://192.168.250.69:9010
## 6、phpmyadmin （docker 安装部署）
使用phpmyadmin作为web端数据库管理工具： http://192.168.250.78:8080
## 7、sonarqube （docker 安装部署）
使用sunarqube作为代码检查： http://192.168.250.78:9000
## 8、Jenkins (tomcat部署war包在/home/apache-tomcat-7.0.73/webapps)
使用Jenkins作为项目CI/CD平台： http://192.168.250.62:9010
## 9、Zabbix (docker 安装部署)
使用Zabbix作为线上环境监控平台： http://121.36.132.116:81 
## 10、Nextcloud (docker-compose部署，docker-compose.yml在nextcloud目录下)
使用nextcloud作为私有云盘并用onlyoffice作为文档服务器：http://192.168.250.77:9010  
使用Nextcloud迁移后的OpenLDAP的服务器为： http://122.112.153.145:81
## 11、EFK
使用EF（filebeat）K作为日志集中平台： http://kibanaprod.letsbim.net
## 12、NPS
使用NPS作为内网穿透工具：  http://nps.letsbim.net



