## 安装lazydocker可视化工具
实时查看docker容器日志和镜像的编辑

## 安装docker-ce
1、如果之前安装过旧版本的Docker请先卸载;首次安装请跳过此步骤。  
yum remove docker docker-common container-selinux  docker-selinux docker-engine  
2、一键安装docker最新版  
curl -fsSL https://get.docker.com/ | sh  
3、安装 yum-utils ,它可以提供 yum-config-manager 来管理repository及扩展包  
yum install -y yum-utils  
4、设置稳定的存储库  
yum-config-manager --add-repo https://download.daocloud.io/docker/linux/centos/docker-ce.repo  
wget -O /etc/yum.repos.d/docker-ce.repo https://mirrors.ustc.edu.cn/docker-ce/linux/centos/docker-ce.repo  
5、安装装最新版本的Docker CE  
yum install docker-ce -y

## 安装go语言环境配置环境变量
wget https://dl.google.com/go/go1.12.6.linux-amd64.tar.gz    
tar -C /usr/local -xzf go1.12.6.linux-amd64.tar.gz    
vim /etc/profile  末尾添加  
export PATH=$PATH:/usr/local/go/bin 
执行 source /etc/profile

## 下载工具并解压运行
wget https://github.com/jesseduffield/lazydocker/releases/download/v0.7.4/lazydocker_0.7.4_Linux_x86_64.tar.gz
tar -zxcf lazydocker_0.7.4_Linux_x86_64.tar.gz   
./lazydocker

## 运行nextcloud
docker-compose up -d  
docker-compose restart

## docker部署OpenLDAP及PHPLdapAdmin
docker run -p 389:389 --restart=always --name openldap --network bridge --hostname openldap --env LDAP_ORGANISATION="sinux" --env LDAP_DOMAIN="sinux.com" --env LDAP_ADMIN_PASSWORD="Sinux_2020" --volume /application/slapd/database:/var/lib/ldap --volume /application/slapd/config:/etc/ldap/slapd.d  --detach osixia/openldap  
docker exec -it openldap bash  
查询ip 使用命令ip a  
docker run -d --privileged -p 8080:80 --restart=always --name phpldapadmin --network bridge --env PHPLDAPADMIN_HTTPS=false
-v /application/config/config.php:/container/service/phpldapadmin/assets/config/config.php  
--env PHPLDAPADMIN_LDAP_HOSTS=192.168.10.122 -d osixia/phpldapadmin

## kubeadm部署K8S
kubeadm init --image-repository registry.aliyuncs.com/google_containers --ignore-preflight-errors=Swap

## docker运行sonar代码检查工具
docker run -d --name sonar -p 9000:9000 --restart=always  sonarqube:lts  
docker run --name myadmin --restart=always -d -e PMA_HOST=192.168.250.78 -e PMA_PORT=3306 -p 8080:80 phpmyadmin/phpmyadmin

## 运行zabbix
参考文章：https://blog.rj-bai.com/post/144.html  
docker run -p 3306:3306 --hostname mysql --restart=always --name debian-mysql-5.7 -t  -e MYSQL_USER="zabbix"  -e MYSQL_DATABASE="zabbix"  -e MYSQL_PASSWORD="passwd"  -e MYSQL_ROOT_PASSWORD="passwd"  -v /data/mysql:/var/lib/mysql -v /home/docker-zabbix/mysql:/etc/mysql/conf.d/ -d swr.cn-east-2.myhuaweicloud.com/jh-soft/zabbix-mysql:v1.0
docker exec -it debian-mysql-5.7 /bin/bash -c 'mysql -uroot -ppasswd'  
grant all privileges on *.* to zabbix@'%' identified by 'passwd';  
flush privileges;  

docker run -v /home/docker-zabbix/zabbix/zabbix_server.conf:/etc/zabbix/zabbix_server.conf --name zabbix-server --restart=always -t  -p 10051:10051  --hostname zabbix-server  -e DB_SERVER_HOST="debian-mysql-5.7"  -e MYSQL_DATABASE="zabbix"  -e MYSQL_USER="zabbix"  -e MYSQL_PASSWORD="passwd"  -e MYSQL_ROOT_PASSWORD="passwd"    --link debian-mysql-5.7:mysql   -d swr.cn-east-2.myhuaweicloud.com/jh-soft/zabbix-server:v1.0  

docker run --name zabbix-web --restart=always -t  -p 81:80  --hostname zabbix-web  -e PHP_TZ="Asia/Shanghai"  -e DB_SERVER_HOST="debian-mysql-5.7"  -e MYSQL_DATABASE="zabbix"  -e MYSQL_USER="zabbix"  -e MYSQL_PASSWORD="passwd"  -e MYSQL_ROOT_PASSWORD="passwd"  --link debian-mysql-5.7:mysql  --link zabbix-server:zabbix-server  -d swr.cn-east-2.myhuaweicloud.com/jh-soft/zabbix-web-nginx:v2.0

## 安装代码审查工具Review Board
http://www.51testing.com/html/28/116228-3712355.html  
注意修改：vim /var/www/reviewboard/conf/settings_local.py   
\#ALLOWED_HOSTS = ['127.0.0.1']    
ALLOWED_HOSTS = ['*']

## 运行docker-elk
cd /root  
git clone https://github.com/deviantony/docker-elk  
docker-compose up -d 

## github优秀的docker-compose项目
git clone https://github.com/JyBigBoss/docker-compose.git

## docker运行nginx
docker run --name nginx -p 9010:80 --restart=always -v /root/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx

## docker运行数据库
docker run -p 6379:6379 --restart=always --name redis -d  hub.c.163.com/library/redis:3.2.9 redis-server --appendonly yes  
docker run -d  -p 27017:27017 --restart=always -v /home/docker/mongodb:/data/db --name mongodb hub.c.163.com/library/mongo:3.4.3  
docker run -itd -p 3306:3306 -e MYSQL_ROOT_PASSWORD=gemhone2016 -v /home/docker/mysql:/var/lib/mysql --restart=always --name mysqld hub.c.163.com/library/mysql:5.6.36

## docker运行nexus3
mkdir -p /home/nexus-data &&  chown -R 200 /home/nexus-data/  
docker run -d -p 9010:8081 --restart=always --name nexus -v /home/nexus-data:/nexus-data sonatype/nexus3  

## docker运行es和kibana
docker network create somenetwork  
docker run -d --name elasticsearchccr -v /home/es/data:/usr/share/elasticsearch/data --restart=always --net somenetwork -p 9201:9200 -p 9301:9300 -e "discovery.type=single-node" -e TAKE_FILE_OWNERSHIP=111 elasticsearch:6.8.1  
docker run -d --name kibana --restart=always --net somenetwork -p 5601:5601 kibana:6.8.1  
### 添加密码认证
docker exec -it elasticsearch /bin/bash  
vim  config/elasticsearch.yml  
exit  退出容器  
docker restart elasticsearch  
docker exec -it elasticsearch /bin/bash  
curl -H "Content-Type:application/json" -XPOST  http://127.0.0.1:9200/_xpack/license/start_trial?acknowledge=true   
elasticsearch-setup-passwords interactive   设置密码  
修改密码 curl -H "Content-Type:application/json" -XPOST -u elastic 'http://127.0.0.1:9200/_xpack/security/user/elastic/_password' -d '{ "password" : "123456" }'  
docker exec -it kibana /bin/bash  
vi config/kibana.yml  添加  

elasticsearch.username: "elastic"  
elasticsearch.password: "123456"  

docker restart kibana 重启容器  
修改filebeat配置文件  vim /etc/filebeat/filebeat.yml  
\#-------------------------- Elasticsearch output ------------------------------  
output.elasticsearch:  
  \# Array of hosts to connect to.  
  hosts: ["192.168.250.78:9200"]  
 \# Optional protocol and basic auth credentials.  
 \#protocol: "https"  
   username: "elastic"   
   password: "123456"    
 
systemctl restart filebeat  
## gitlab-docker迁移
原服务器执行：docker exec gitlab gitlab-rake gitlab:backup:create  
现服务器执行：scp root@114.116.4.185:/srv/gitlab/data/backups/* /home/gitlab/data/backups  
chmod -R 777 /home/gitlab/data/backups  
docker run -d --publish 443:443 --publish 80:80 --publish 23:22 --name gitlab --restart always --volume /home/gitlab/config:/etc/gitlab --volume /home/gitlab/logs:/var/log/gitlab --volume /home/gitlab/data:/var/opt/gitlab docker.io/gitlab/gitlab-ce:11.5.3-ce.0  
docker exec -it gitlab gitlab-rake  gitlab:backup:restore BACKUP=1561910486_2019_06_30_11.5.3

## nextcloud-docker-compose迁移
复制docker-compose.yml和nextcloud目录到现服务器的/home/nexcloud目录下  
chmod 777 -Rf /home/nextcloud/nextcloud/www/ 
编辑/home/nextcloud/nextcloud/www/config/config.php修改IP地址  
执行命令：docker-compose start

## ssh免密登录
1、服务器客户端：ssh-keygen -b 1024 -t dsa 一直回车   
2、将公钥上传到欲远程连接的主机上面      
ssh-copy-id root@122.112.204.11   
autossh内网穿透：autossh -M 8088 -NfR  9009:localhost:80 root@122.112.204.11

## 运维监控
研究院云桌面：https://118.122.122.42:666  
垒知云知了：http://www.letsbim.net  
谷歌搜索：https://www.vultr.com/?ref=8140596  
ss:  wget -N --no-check-certificate https://raw.githubusercontent.com/ToyoDAdoubiBackup/doubi/master/ss-go.sh && chmod +x ss-go.sh && bash ss-go.sh    
ssr: wget --no-check-certificate -O shadowsocks-all.sh https://raw.githubusercontent.com/teddysun/shadowsocks_install/master/shadowsocks-all.sh    
     chmod +x shadowsocks-all.sh  
     ./shadowsocks-all.sh 2>&1 | tee shadowsocks-all.log    
bbr加速：wget -N --no-check-certificate "https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh" && chmod +x tcp.sh && ./tcp.sh



