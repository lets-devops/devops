-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.22-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  10.3.0.5855
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 officialwebsite 的数据库结构
CREATE DATABASE IF NOT EXISTS `officialwebsite` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;
USE `officialwebsite`;

-- 导出  表 officialwebsite.admin 结构
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.admin 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
REPLACE INTO `admin` (`id`, `login_date`, `password`, `username`) VALUES
	(1, '2020-03-12 16:03:54', '$2a$10$9Jl//gRxrAx85s8ep2qxg.e.430ZqYWmLYLlDIQqsYuvL34X2SU8G', 'admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- 导出  表 officialwebsite.banner 结构
DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `img_path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.banner 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
REPLACE INTO `banner` (`id`, `create_date`, `img_path`) VALUES
	(1, '2020-03-31 02:22:37', '/img/049bcb9c58dd4cfc855601fb9c3f12b3.jpg'),
	(2, '2020-03-31 02:22:47', '/img/2efab09954f84240b8c33af6239d4a38.jpg'),
	(3, '2020-03-31 02:22:57', '/img/62f71e6564374adf8b10793a05977176.jpg');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- 导出  表 officialwebsite.dynamic 结构
DROP TABLE IF EXISTS `dynamic`;
CREATE TABLE IF NOT EXISTS `dynamic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8mb4_bin,
  `title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `is_effective` bit(1) NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.dynamic 的数据：~24 rows (大约)
/*!40000 ALTER TABLE `dynamic` DISABLE KEYS */;
REPLACE INTO `dynamic` (`id`, `content`, `title`, `create_date`, `is_effective`, `image_path`, `type`) VALUES
	(25, '<p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">2018年3月，美国总统特朗普签署备忘录，发布“301调查”报告，宣称中国在技术转移、知识产权和创新方面的法律、政策和做法是“不合理的”，造成了美国商业的负担和损失。美国依据相关调查结果，拟对从中国进口的商品大规模加征关税，并向世界贸易组织起诉中国。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">随后，2018年9月6日下午举行的商务部例行发布会上，有媒体提问时指出，美拟对华2000亿输美商品加征关税，商务部明确回应将反制。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;随后中国与美国之间展开了相互加征关税的措施，期间还进行了贸易磋商。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;随后美国又单方面撕毁贸易磋商协议，并对中国相关企业（中兴以及华为，阿里巴巴等）实施相应的业务限制以及禁售芯片等措施。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">在整个中美两国贸易战期间，美方认为，美中贸易逆差是由两国之间贸易存在不平等现象导致的，尤其是在知识产权方面。同时美方甚至一些西方国家，也多次抹黑我国知识产权方面的发展和进步，并指责我国知识产权保护制度。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">作为一名从事知识产权工作的相关人员，我们认为我国知识产权制度等相关方面确实存在一些问题，但是这些问题是有待于相关法律法规约束且完善的问题，并不是像西方国家所的根本性制度问题。同时，作为从业人员，我们应当认识到中国知识产权事业以及制度在过去40年内确实有了飞速的发展，各行各业的技术人员以及企业对于知识产权的保护意识也逐渐增强。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">下面我们就从以下几个方面，介绍一下近些年我国知识产权事业的发展与取得的进步：</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">1.&nbsp;商标尤其</span><strong style="color: rgb(51, 51, 51);">专利申请量全球第一。</strong></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;数据显示，2017年：</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;全年发明专利申请量达到138.2万件，同比增长14.2%，连续7年居世界首位。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;PCT国际专利申请受理量5.1万件，同比增长12.5%，排名跃居全球第二；每万人口发明专利拥有量达到9.8件。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;受理商标注册申请574.8万件，同比增长55.72%，连续16年居世界第一；累计有效商标注册1492万件。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;我国申请人提交马德里商标国际注册申请4810件，排名全球第三。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;作品、计算机软件著作权登记量分别达到200.2万件、74.54万件，同比分别增长25.15%、82.79%。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;农业、林业植物新品种权申请量分别达到3842件、623件。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;地理标志保护产品数量稳步增长。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<strong style="color: rgb(51, 51, 51);">2.&nbsp;不断强化知识产权制度以及相关法规。</strong></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">经过调阅相关资料发现，中国近两年不断地对知识产权相关法规开展完善工作。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">举例说明，《反不正当竞争法》经全国人民代表大会常务委员会第三十次会议修订通过，《专利法》《专利代理条例》《人类遗传资源管理条例》修改和制定工作取得实质性进展。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">同时2017年专利局公告显示，我国正在积极推进《著作权法》《奥林匹克标志保护条例》《国防专利条例》《植物新品种保护条例》《生物遗传资源获取与惠益分享管理条例》制修订工作，推动知识产权领域反垄断执法指南尽快出台，深化对新领域、新业态创新成果知识产权保护制度的研究等工作。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<strong style="color: rgb(51, 51, 51);">3.&nbsp;不断加强了知识产权的保护力度。</strong></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">中国近些年成立了知识产权法院为专利维权以及专利侵权判定提供了法律平台。其中：</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2017年专利行政执法办案量6.7万件，同比增长36.3%；</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商标行政执法办案量3.01万件，涉案金额3.33亿元；</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;版权部门查处侵权盗版案件3100余件，收缴盗版品605万件；</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;海关查获进出口侵权货物1.92万批次，涉及侵权货物4095万件，案值1.82亿元；</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">·&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全国法院新收知识产权民事、行政、刑事一审案件21.35万件，审结20.30万件，分别同比增长40.37%、38.38%。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<strong style="color: rgb(51, 51, 51);">4.&nbsp;将惩罚性赔偿制度引入到知识产权保护领域。</strong></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">2018年3月5日，第十三届全国人民代表大会第一次会议在人民大会堂开幕。国务院总理李克强代表国务院向大会作政府工作报告，指出：</span></p><p class="ql-align-justify">	<em style="color: rgb(51, 51, 51);">“要以保护产权、维护契约、统一市场、平等交换、公平竞争为基本导向，完善相关法律法规，对各种侵权行为要依法严肃处理，对产权纠纷案件要依法甄别纠正，强化知识产权保护，实行侵权惩罚性赔偿制度。”</em></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">随后，3月13日十三届全国人大一次会议在北京举行“部长通道”采访活动。国家知识产权局局长申长雨表示，加快推动《专利法》修改，引入惩罚性赔偿措施，加大对各类侵权行为惩治力度，让侵权者付出沉重代价。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">同时，中国国家主席习近平也明确指出，产权保护特别是知识产权保护是塑造良好营商环境的重要方面。</span><strong style="color: rgb(51, 51, 51);">强化知识产权保护不仅是扩大对外开放的需要，更是中国自身发展的需要。</strong></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">党的十八大以来，我国以前所未有的力度加强知识产权保护，正致力于贯彻实施严格的知识产权保护政策，推动提高法定赔偿上限，增设惩罚性赔偿制度，设立知识产权专门法院，加强重点领域行政执法，积极开展仲裁调解等社会治理。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">国家知识产权局有关调查表明，中国知识产权保护社会满意度已经由2012年的63.69分提升到2016年的72.38分，保护成效得到了国内外普遍认可，也吸引了各国创新主体积极申请中国专利。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">以美国为例，2017年，美国获得23679件中国专利授权，在各国排名第二，美国高通公司则成为2017年获得中国专利权最多的外国企业。说中国没有很好地保护美国企业知识产权，显然难以自圆其说。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">作为从业多年的知识产权工作者，我们切实感受到了国家对于知识产权制度与发展的重视，切实体会到了我们日常工作的重要性和责任。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">对于一名普通的知识产权从业人员，我们面对这样的无端指责和诽谤，只能强化自身学习，不断地利用专业知识将专利制度以及知识产权保护意识传播给社会，为经济转型出一份力。</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">&nbsp;</span></p><p class="ql-align-justify">	<span style="color: rgb(51, 51, 51);">知识产权制度的目标是</span><strong style="color: rgb(51, 51, 51);">有效保护创新成果，维持市场秩序，实现公平竞争，同时也要推动技术转让和传播，使创新成果的福祉为人类所共享。</strong><span style="color: rgb(51, 51, 51);">同时，我们还认为知识产权应是世界各国之间创新合作的桥梁，而不能成为贸易保护主义的大棒，更不能拿来用作遏制他国发展的武器。</span></p>', '贸易保护主义下中国的知识产权发展以及进步见闻', '2020-03-28 01:28:28', b'1', '/img/4a7bfa6b13da4e4b969751954e326fc2.jpg', 1),
	(26, '<p>	<span style="color: rgb(89, 89, 89);">为进一步减轻社会负担，促进专利创造保护，根据《财政部国家发展改革委关于停征、免征和调整部分行政事业性收费有关政策的通知》精神，国家知识产权局于今年8月1日起停征和调整部分专利收费。据介绍，这是国家知识产权局贯彻落实深化“放管服”改革部署要求，加大简政放权力度，落实好相关利企便民举措的重要具体措施之一。</span></p><p>	</p><p>	<span style="color: rgb(89, 89, 89);">据了解，此次停征和调整部分专利收费主要涉及三方面：一是停征专利收费（国内部分）中的专利登记费、公告印刷费、著录事项变更费（专利代理机构、代理人委托关系的变更），《专利合作条约》（PCT）专利申请收费（国际阶段部分）中的传送费。而对于缴费期限届满日在2018年7月31日（含）前的上述费用，应按原规定缴纳。二是对符合《专利收费减缴办法》（财税〔2016〕78号）有关条件的专利申请人或者专利权人，专利年费的减缴期限由自授权当年起6年内，延长至10年内。对于2018年7月31日（含）前已准予减缴的专利，作如下处理：处于授权当年起6年内的，年费减缴期限延长至第10年；处于授权当年起7—9年的，自下一年度起继续减缴年费直至10年；处于授权当年起10年及10年以上的，不再减缴年费。三是对进入实质审查阶段的发明专利申请，在第一次审查意见通知书答复期限届满前（已提交答复意见的除外）主动申请撤回的，可以请求退还50%的专利申请实质审查费。</span></p><p>	</p><p>	<span style="color: rgb(89, 89, 89);">据悉，根据上述调整，国家知识产权局目前已对费用减缴请求书、意见陈述书（关于费用）等请求类表格作了修改，并于8月1日起正式启用，旧版表格同时停用。</span></p><p>	</p>', '国家知识产权局停征和调整部分专利收费1', '2020-03-28 06:43:47', b'1', '/img/50cde9e0816347cd9085333f323b087f.jpg', 1),
	(27, '<p>	<span style="color: rgb(89, 89, 89);">为进一步减轻社会负担，促进专利创造保护，根据《财政部国家发展改革委关于停征、免征和调整部分行政事业性收费有关政策的通知》精神，国家知识产权局于今年8月1日起停征和调整部分专利收费。据介绍，这是国家知识产权局贯彻落实深化“放管服”改革部署要求，加大简政放权力度，落实好相关利企便民举措的重要具体措施之一。</span></p><p>	</p><p>	<span style="color: rgb(89, 89, 89);">据了解，此次停征和调整部分专利收费主要涉及三方面：一是停征专利收费（国内部分）中的专利登记费、公告印刷费、著录事项变更费（专利代理机构、代理人委托关系的变更），《专利合作条约》（PCT）专利申请收费（国际阶段部分）中的传送费。而对于缴费期限届满日在2018年7月31日（含）前的上述费用，应按原规定缴纳。二是对符合《专利收费减缴办法》（财税〔2016〕78号）有关条件的专利申请人或者专利权人，专利年费的减缴期限由自授权当年起6年内，延长至10年内。对于2018年7月31日（含）前已准予减缴的专利，作如下处理：处于授权当年起6年内的，年费减缴期限延长至第10年；处于授权当年起7—9年的，自下一年度起继续减缴年费直至10年；处于授权当年起10年及10年以上的，不再减缴年费。三是对进入实质审查阶段的发明专利申请，在第一次审查意见通知书答复期限届满前（已提交答复意见的除外）主动申请撤回的，可以请求退还50%的专利申请实质审查费。</span></p><p>	</p><p>	<span style="color: rgb(89, 89, 89);">据悉，根据上述调整，国家知识产权局目前已对费用减缴请求书、意见陈述书（关于费用）等请求类表格作了修改，并于8月1日起正式启用，旧版表格同时停用。</span></p><p>	</p>', '国家知识产权局停征和调整部分专利收费2', '2020-03-28 06:46:36', b'1', '/img/802333c3af7d42b5a7d47533d4da544a.jpg', 1),
	(28, '<p>	<span style="color: rgb(89, 89, 89);">为进一步减轻社会负担，促进专利创造保护，根据《财政部国家发展改革委关于停征、免征和调整部分行政事业性收费有关政策的通知》精神，国家知识产权局于今年8月1日起停征和调整部分专利收费。据介绍，这是国家知识产权局贯彻落实深化“放管服”改革部署要求，加大简政放权力度，落实好相关利企便民举措的重要具体措施之一。</span></p><p>	</p><p>	<span style="color: rgb(89, 89, 89);">据了解，此次停征和调整部分专利收费主要涉及三方面：一是停征专利收费（国内部分）中的专利登记费、公告印刷费、著录事项变更费（专利代理机构、代理人委托关系的变更），《专利合作条约》（PCT）专利申请收费（国际阶段部分）中的传送费。而对于缴费期限届满日在2018年7月31日（含）前的上述费用，应按原规定缴纳。二是对符合《专利收费减缴办法》（财税〔2016〕78号）有关条件的专利申请人或者专利权人，专利年费的减缴期限由自授权当年起6年内，延长至10年内。对于2018年7月31日（含）前已准予减缴的专利，作如下处理：处于授权当年起6年内的，年费减缴期限延长至第10年；处于授权当年起7—9年的，自下一年度起继续减缴年费直至10年；处于授权当年起10年及10年以上的，不再减缴年费。三是对进入实质审查阶段的发明专利申请，在第一次审查意见通知书答复期限届满前（已提交答复意见的除外）主动申请撤回的，可以请求退还50%的专利申请实质审查费。</span></p><p>	</p><p>	<span style="color: rgb(89, 89, 89);">据悉，根据上述调整，国家知识产权局目前已对费用减缴请求书、意见陈述书（关于费用）等请求类表格作了修改，并于8月1日起正式启用，旧版表格同时停用。</span></p><p>	</p>', '国家知识产权局停征和调整部分专利收费3', '2020-03-28 06:48:10', b'1', '/img/0771cc7bb5c04560b19713de7cb6931d.jpg', 1);
/*!40000 ALTER TABLE `dynamic` ENABLE KEYS */;

-- 导出  表 officialwebsite.image 结构
DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.image 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;

-- 导出  表 officialwebsite.leave_comments 结构
DROP TABLE IF EXISTS `leave_comments`;
CREATE TABLE IF NOT EXISTS `leave_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `emil_or_phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.leave_comments 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `leave_comments` DISABLE KEYS */;
REPLACE INTO `leave_comments` (`id`, `content`, `emil_or_phone`, `name`, `create_date`) VALUES
	(1, 'aksjakfjaa;slfja;gja;dkghslfjadlkgjalgkhalg', '123456', '123456', '2020-03-23 10:22:58');
/*!40000 ALTER TABLE `leave_comments` ENABLE KEYS */;

-- 导出  表 officialwebsite.link 结构
DROP TABLE IF EXISTS `link`;
CREATE TABLE IF NOT EXISTS `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `cerate_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.link 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
REPLACE INTO `link` (`id`, `name`, `path`, `cerate_date`) VALUES
	(2, '廖氏科技', 'www.lskj.com', '2020-03-23 04:21:50');
/*!40000 ALTER TABLE `link` ENABLE KEYS */;

-- 导出  表 officialwebsite.log 结构
DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `operation_content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `operator` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.log 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- 导出  表 officialwebsite.recruit_info 结构
DROP TABLE IF EXISTS `recruit_info`;
CREATE TABLE IF NOT EXISTS `recruit_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `education` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `experience` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_effective` bit(1) NOT NULL,
  `salary` double NOT NULL,
  `work` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.recruit_info 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `recruit_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `recruit_info` ENABLE KEYS */;

-- 导出  表 officialwebsite.server 结构
DROP TABLE IF EXISTS `server`;
CREATE TABLE IF NOT EXISTS `server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_effective` bit(1) NOT NULL,
  `serial` int(11) NOT NULL,
  `server_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `server_id` int(11) DEFAULT NULL,
  `url` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK261iscg0w1d6ps5144pspi787` (`server_id`),
  CONSTRAINT `FK261iscg0w1d6ps5144pspi787` FOREIGN KEY (`server_id`) REFERENCES `server` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.server 的数据：~9 rows (大约)
/*!40000 ALTER TABLE `server` DISABLE KEYS */;
REPLACE INTO `server` (`id`, `is_effective`, `serial`, `server_name`, `server_id`, `url`, `path`) VALUES
	(1, b'1', 1, '知识产权', NULL, '/intellectualProperty', NULL),
	(2, b'1', 2, '工商财税', NULL, '/businessFinanceTax', NULL),
	(3, b'1', 3, '科技服务', NULL, '/scienceService', NULL),
	(4, b'1', 4, '科宝申报', NULL, '/copalDeclare', NULL),
	(5, b'1', 5, '品牌服务', NULL, '/brandService', NULL),
	(6, b'1', 6, '计算机IT', NULL, '/computerIT', NULL),
	(7, b'1', 7, '移动智能', NULL, '/mobileIntelligence', NULL),
	(8, b'1', 8, '法律法规', NULL, '/law', NULL),
	(9, b'1', 9, '其他服务', NULL, '/other', NULL);
/*!40000 ALTER TABLE `server` ENABLE KEYS */;

-- 导出  表 officialwebsite.webinfo 结构
DROP TABLE IF EXISTS `webinfo`;
CREATE TABLE IF NOT EXISTS `webinfo` (
  `id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `identification_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `record_number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `telephone` bigint(20) NOT NULL,
  `copyright` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `technical_support` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `hotline` bigint(20) NOT NULL,
  `qr_code_path` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- 正在导出表  officialwebsite.webinfo 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `webinfo` DISABLE KEYS */;
REPLACE INTO `webinfo` (`id`, `address`, `email`, `identification_code`, `name`, `record_number`, `telephone`, `copyright`, `technical_support`, `hotline`, `qr_code_path`, `logo`) VALUES
	(1, '中国（四川）自由贸易试验区成都高新区南华路1266号4栋1单元8层829号', '28317771909@qq.com', 'XXXXXXX', '胜者为王知识产权代理有限公司', 'XXXXXXX', 18780228092, '胜者为王知识产权代理有限公司', '四川省廖氏科技有限公司', 2124556545, '/img/9bcfa9410db84e959746126b206cac7d.png', '/img/d09dfe75fc4d476ba10466bf0ef54bf1.jpg');
/*!40000 ALTER TABLE `webinfo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
