#!/bin/bash

#while true
#do
#if [ $(netstat -tlnp|grep nginx|wc -l) -ne 1 ]
#then
#    systemctl stop keepalived
#fi
#sleep 2
#done
#run=`lsof -i:80|awk '{print $2}'|wc -l`
#if [ $run -eq 0 ];then
#        killall keepalived
#fi
A=`ps -C nginx --no-header |wc -l`        
if [ $A -eq 0 ];then    #如果nginx没有启动就启动nginx                        
      systemctl start nginx                #重启nginx
      if [ `ps -C nginx --no-header |wc -l` -eq 0 ];then    #nginx重启失败，则停掉keepalived服务，进行VIP转移
              killall keepalived                    
      fi
fi

