#!/bin/bash
pkg="lets-admin"

#git   pull  

_common(){
    #mkdir -p /tmp/${pkg}/
    #npm install
    yarn
    #pm2 stop ./server/index.js
    pm2 stop ${pkg}
    pm2 delete ${pkg}
    #ps -ef|grep ${pkg}|grep -v grep|awk '{print $2}'|xargs kill -9
}

_dev(){
    _common
    npm run dev > /tmp/${pkg}/app.log 2>&1 &
}


_test(){
    _common
    npm run build
    sleep 2
    #pm2 start npm -- run start
    #pm2 start ./server/index.js
    pm2 start npm --name ${pkg} -- run start
}

_prod(){
    _common
    npm run build
    sleep 2
    #pm2 start npm -- run start
    #pm2 start ./server/index.js
    pm2 start npm --name ${pkg} -- run start
}

case "$1" in
    dev)
        _dev
        ;;
    test)
        _test
        ;;
    prod)
        _prod
        ;;
    *)
        echo $"Usage: $0 {dev|test|prod}"
        exit 2
esac

exit 0
