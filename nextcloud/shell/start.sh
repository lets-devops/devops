#!/bin/sh
cd /root/letsbim/lzht
pid=`ps -ef |grep  $1 |grep java |awk '{print $2}'`
if [ -n "$pid" ]
then
   echo "kill -9 " $pid
   kill -9 $pid
fi
nohup java  -jar $1.jar > nohup.out 2>&1  & sleep 1
timeout 30s tail -f nohup.out & sleep 1 
sleep 30
error=`grep Exception nohup.out |wc -l`
if [ $error -gt 0 ]
then
 exit -1
else
 exit 0
fi
