# 华为云申请虚拟VIP,绑定服务器和弹性公网IP
华为云上要申请此vip地址并绑定公网ip和nginx调度服务器（注意安全组规则，允许外网访问）

# 安装keepalived
yum install -y keepalved

# 编辑监控nginx是否运行的脚本nginx_check.sh
#!/bin/bash  

#while true  
#do  
#if [ $(netstat -tlnp|grep nginx|wc -l) -ne 1 ]  
#then  
\#    systemctl stop keepalived  
#fi  
#sleep 2  
#done  

run=`lsof -i:80|awk '{print $2}'|wc -l`  
if [ $run -eq 0 ];then  
        killall keepalived  
fi  

# 编辑Mastar主机 /etc/keepalived/keepalived.conf

! Configuration File for keepalived  

global_defs {  
   notification_email {  
     acassen@firewall.loc  
     failover@firewall.loc  
     sysadmin@firewall.loc  
   }  
   notification_email_from Alexandre.Cassen@firewall.loc  
   smtp_server 192.168.200.1  
   smtp_connect_timeout 30  
   router_id lb01  
}  

vrrp_script chk_nginx {  
        script "/root/nginx_check.sh"  
        interval 2  
}  

vrrp_instance VI_1 {  
    state MASTER  
    interface eth0  
    virtual_router_id 55  
    mcast_src_ip 192.168.50.148  
    priority 150  
    advert_int 1  
    authentication {  
        auth_type PASS  
        auth_pass 123456  
    }  
  virtual_ipaddress {  
        192.168.50.60/24  
    }  
  track_script {  
       chk_nginx  
    }  
}  

# 编辑BACKUP主机 /etc/keepalived/keepalived.conf

! Configuration File for keepalived

global_defs {
   notification_email {
     acassen@firewall.loc
     failover@firewall.loc
     sysadmin@firewall.loc
   }
   notification_email_from Alexandre.Cassen@firewall.loc
   smtp_server 192.168.200.1
   smtp_connect_timeout 30
   router_id lb02
}

vrrp_script chk_nginx {
        script "/root/nginx_check.sh"
        interval 2
}
vrrp_instance VI_1 {
    state BACKUP
    interface eth0
    virtual_router_id 55
    mcast_src_ip 192.168.50.135
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 123456
    }
  virtual_ipaddress {
       192.168.50.60/24
    }
  track_script {
       chk_nginx
     }
}

# 启动keepalived
systemctl satrt keepalived  
systemctl enable keepalived

# 运行k8s
kubeadm init --kubernetes-version=1.18.0 --apiserver-advertise-address=192.168.250.134  --image-repository registry.aliyuncs.com/google_containers  --service-cidr=10.10.0.0/16 --pod-network-cidr=10.122.0.0/16  



